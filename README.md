#Project Name - PageScanner
A Standalone Java application build on Java 1.8 / Maven

#OBJECTIVE :

A Java based application that scrapes the Sainsbury�s grocery site and prints a JSON array of all the products on the page.

#HOW TO RUN THE PROGRAM :

The below instructions are based on assumption that the application is running on a Windows OS.

#Pre-requisites

JRE 1.8  

Have maven installed (including maven set in system path variable)  

Have GIT Installed  


#Steps to run the application (COMMAND PROMPT)

Git clone the project to local drive https://kapricanon@bitbucket.org/kapricanon/pagescanner.git  

Open a command prompt  

Navigate to the folder where the project is cloned (to where pom.xml is located)  

Run the command "mvn package"   

Now run the command java -jar target\page-scanner-0.0.1-SNAPSHOT-jar-with-dependencies.jar <fully encoded URL>  
