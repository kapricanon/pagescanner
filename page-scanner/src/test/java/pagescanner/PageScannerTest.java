package pagescanner;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.webapp.WebAppContext;

/**
 * Unit test for simple App.
 * @author Kashyap
 */

public class PageScannerTest 
{	
	private static Server server;
	private static final String ZERO_PRODUCTS_RESOURCE_URL = "http://localhost:8081/ZeroProductsMainPage.html";
	private static final String TWO_PRODUCTS_RESOURCE_URL = "http://localhost:8081/MainPage.html";
	
	
	/**
	 * Starts up a Jetty server to load local resource files 
	 * @throws Exception
	 */
	@BeforeClass
	public static void startServer() throws Exception  {
		server = new Server(8081);
		server.setStopAtShutdown(true);
		WebAppContext webAppContext = new WebAppContext();
		webAppContext.setContextPath("/");
		webAppContext.setResourceBase("src/test/resources");
		webAppContext.setClassLoader(PageScanner.class.getClassLoader());
		server.addHandler(webAppContext);
		server.start();
	}

	@AfterClass
	public static void shutdownServer() throws Exception {
		server.stop();
	}
	
	@Test
	public void testAProductPageWithZeroProducts() throws ParseException, IOException {
		PageScanner pageScanner = new PageScanner();
		List<Product> products = pageScanner.scan(ZERO_PRODUCTS_RESOURCE_URL);
		List<Product> testProducts = Collections.emptyList();
		assertEquals(testProducts, products);
	}
	
	@Test
	public void testAProductPageWithTwoProducts() throws ParseException, IOException {
		PageScanner pageScanner = new PageScanner();
		List<Product> products = pageScanner.scan(TWO_PRODUCTS_RESOURCE_URL);
		List<Product> testProducts = new ArrayList<Product>();
		testProducts.add(new Product("Sainsbury's Apricot Ripe & Ready x5", 2.00, "2kb", "Apricots"));
		testProducts.add(new Product("Sainsbury's Avocado Ripe & Ready XL Loose 300g", 1.50, "2kb", "Avocados"));
		assertEquals(testProducts, products);
	}
	
}