package pagescanner;


import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 * @author Kashyap
 * This test will compare the JSON output from the printProductsAsJson method with the expected result
 * 
 */

public class ProductPrinterTest {

	private final ByteArrayOutputStream sysOutput = new ByteArrayOutputStream();
	private static final String EXPECTED_OUTPUT_FOR_ZERO_PRODUCTS = "{\n\"results\":[],\n\"total\":0.0\n}\n";
	private static final String EXPECTED_OUTPUT_FOR_ONE_PRODUCTS = "{\n\"results\":[{\n\"title\":\"Product1\",\n\"size\":\"2kb\",\n\"unit_price\": 2.5,\n\"description\":\"Product 1\"\n}],\n\"total\":2.5\n}\n";
	private static final String EXPECTED_OUTPUT_FOR_TWO_PRODUCTS = "{\n\"results\":[{\n\"title\":\"Product1\",\n\"size\":\"2kb\",\n\"unit_price\": 2.5,\n\"description\":\"Product 1\"\n},{\n\"title\":\"Product2\",\n\"size\":\"2kb\",\n\"unit_price\":1.5,\n\"description\":\"Product 2\"\n}],\n\"total\":4.0\n}\n";
	private static final String EXPECTED_OUTPUT_FOR_THREE_PRODUCTS = "{\n\"results\":[{\n\"title\":\"Product1\",\n\"size\":\"2kb\",\n\"unit_price\": 2.5,\n\"description\":\"Product 1\"\n},{\n\"title\":\"Product2\",\n\"size\":\"2kb\",\n\"unit_price\":1.5,\n\"description\":\"Product 2\"\n},{\n\"title\":\"Product3\",\n\"size\":\"2kb\",\n\"unit_price\":4.5,\n\"description\":\"Product 3\"\n}],\n\"total\":8.5\n}\n";

	@Before
	public void setUpStreams() {
	    System.setOut(new PrintStream(sysOutput));
	}

	@After
	public void cleanUpStreams() {
	    System.setOut(null);
	}
	
	@Test
	public void testJsonOutputOfZeroProducts() throws JsonGenerationException, JsonMappingException, IOException {
		List<Product> products = new ArrayList<Product>();
	    new ProductPrinter().printProductsAsJson(products);
		assertEquals(sysOutput.toString().replaceAll("\\s+",""), EXPECTED_OUTPUT_FOR_ZERO_PRODUCTS.replaceAll("\\s+",""));
	}
	
	@Test
	public void testJsonOutputOfOneProduct() throws JsonGenerationException, JsonMappingException, IOException {
		List<Product> products = new ArrayList<Product>();
		products.add(new Product("Product1", 2.50, "2kb", "Product 1"));
	    new ProductPrinter().printProductsAsJson(products);
		assertEquals(sysOutput.toString().replaceAll("\\s+",""), EXPECTED_OUTPUT_FOR_ONE_PRODUCTS.replaceAll("\\s+",""));
	}
	
	@Test
	public void testJsonOutputOfTwoProducts() throws JsonGenerationException, JsonMappingException, IOException {
		List<Product> products = new ArrayList<Product>();
		products.add(new Product("Product1", 2.50, "2kb", "Product 1"));
		products.add(new Product("Product2", 1.50, "2kb", "Product 2"));
	    new ProductPrinter().printProductsAsJson(products);
		assertEquals(sysOutput.toString().replaceAll("\\s+",""), EXPECTED_OUTPUT_FOR_TWO_PRODUCTS.replaceAll("\\s+",""));
	}
	
	@Test
	public void testJsonOutputOfThreeProducts() throws JsonGenerationException, JsonMappingException, IOException {
		List<Product> products = new ArrayList<Product>();
		products.add(new Product("Product1", 2.50, "2kb", "Product 1"));
		products.add(new Product("Product2", 1.50, "2kb", "Product 2"));
		products.add(new Product("Product3", 4.50, "2kb", "Product 3"));
	    new ProductPrinter().printProductsAsJson(products);
		assertEquals(sysOutput.toString().replaceAll("\\s+",""), EXPECTED_OUTPUT_FOR_THREE_PRODUCTS.replaceAll("\\s+",""));
	}
}
