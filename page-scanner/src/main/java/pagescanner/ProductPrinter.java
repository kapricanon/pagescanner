package pagescanner;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

public class ProductPrinter {
	
	/**
	 * 
	 * @param products - A list of products to be printed to StdOut in JSON format
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public void printProductsAsJson(List<Product> products) throws JsonGenerationException, JsonMappingException, IOException {
		Map<String, Object> printMap = new LinkedHashMap<String, Object>();
		printMap.put("results", products);
		printMap.put("total", calculateTotalPrice(products));
		ObjectMapper mapper = new ObjectMapper();
		System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(printMap));
	}
	
	/**
	 * 
	 * @param products - A list of products whose total price needs to be calculated
	 * @return Sum of the unit_price of each product
	 */
	public double calculateTotalPrice(List<Product> products) {
		return products.stream().mapToDouble(o -> o.getUnit_price()).sum();
	}
}
