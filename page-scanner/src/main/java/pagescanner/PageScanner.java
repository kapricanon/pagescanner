package pagescanner;

import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * 
 * @author Kashyap
 *
 * A utility class to scrape list of Products from the given URL
 */

public class PageScanner {
	
	private static final String PRODUCT_TITLE_CLASS_NAME = ".productTitleDescriptionContainer";
	private static final String PRODUCT_DATA_ITEM_HEADER_CLASS_NAME = ".productDataItemHeader";
	private static final String PRODUCT_UNIT_PRICE_CLASS_NAME = ".pricePerUnit";
	private static final String PRODUCT_LINK_CLASS_NAME = ".productInfo h3 a";
	
	/**
	 * 
	 * @param url - The URL to scan
	 * @return A list of Products with the required information i.e. Title, Unit Price, Description & Size of the page
	 * @throws IOException 
	 * @throws ParseException
	 */
	public List<Product> scan(String url) throws IOException, ParseException {
		return findProducts(Jsoup.connect(url).get());
	}
	
	/**
	 * 
	 * @param doc Expects a Document object read from the URL which needs to be analysed
	 * @return A list of Products found on scanning the Document
	 * @throws ParseException
	 * @throws IOException
	 */
	public List<Product> findProducts(Document doc) throws ParseException, IOException {
		Elements productLinks = findProductLinks(doc);
		List<Product> products = new ArrayList<Product>();
		
		for(Element link : productLinks) {
			products.add(populateProductDetailsFromPage(Jsoup.connect(link.attr("abs:href")).get()));
		}
		return products;
		
	}
	
	/**
	 * 
	 * @param document - A document object
	 * @return A populated Product object
	 * @throws ParseException
	 */
	private Product populateProductDetailsFromPage(Document document) throws ParseException {
		String title = document.select(PRODUCT_TITLE_CLASS_NAME).text();
		String description = null;
		String size = (document.body().text().length() / 1024) + "kb";
		List<Element> productDataItemHeaders = document.select(PRODUCT_DATA_ITEM_HEADER_CLASS_NAME);
		for (Element element : productDataItemHeaders) {
			if(element.text().equalsIgnoreCase("Description")) {
					description = element.nextElementSibling().text();
			}
		}
		final String unitPriceText = document.select(PRODUCT_UNIT_PRICE_CLASS_NAME).get(0).text();
		double unitPrice = NumberFormat.getInstance().parse(unitPriceText.substring(unitPriceText.indexOf("£") + 1)).doubleValue();

		return new Product(title, unitPrice, size, description);
	}

	/**
	 * 
	 * @param doc - A document object
	 * @return  Elements containing URL links for all the elements found on the page
	 */
	public Elements findProductLinks(Document doc) {
		Elements links = doc.select(PRODUCT_LINK_CLASS_NAME);
        return links;
	}

}
