package pagescanner;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * 
 * @author Kashyap
 *
 */

public class PageScannerMain 
{
	/**
	 * Application entry point: Only one command prompt parameter is expected. The parameter should be a URL of the page which needs scanning
	 * 
	 * @param args Parameter passed when starting/running the program. Only one expected and must be a URL which needs to be scanned.
	 * @throws IOException 
	 * @throws ParseException 
	 * 
	 */
    public static void main( String[] args ) throws IOException, ParseException
    {
    	if (args.length != 1) {
			System.out.println("Please enter only one argument which is a URL that needs to be scanned.");
			System.exit(1);
		}
    	System.out.println("\n\nScanning the URL : " + args[0]);
    	PageScanner pageScanner = new PageScanner();
    	List<Product> products = pageScanner.scan(args[0]);
    	new ProductPrinter().printProductsAsJson(products);
    }
}
